# Example of running out of memory using Dask

## Instructions to execute

1. clone this repo
1. Install the dependencies:
    1. `python3 -m venv venv`
    1. `source venv/bin/activate`
    1. `pip install -r requeriments.txt`
1. Run script: `python3 main.py`